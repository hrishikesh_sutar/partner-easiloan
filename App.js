import React ,{Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import LoginPage from './Pages/Login';
import SignupPage from './Pages/Signup';

const  App=createStackNavigator({
  LoginPage:{
    screen:LoginPage
  },
  SignupPage:{
    screen:SignupPage 
  }
});

export default createAppContainer(App);