import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  TouchableHighlight,
} from "react-native";
import { Image } from "react-native";
import { Card } from "react-native-shadow-cards";
import { Component } from "react/cjs/react.production.min";

export default class LoginPage extends Component {
  Show = () => {
    this.props.navigation.navigate("SignupPage");
  };
  render() {
    return (
      <View>
        <View style={styles.container}>
          <Image
            source={require("../assets/Easy-Loan.jpg")}
            style={{
              flex: 1,
              height: 50,
              marginTop: 35,
              backgroundColor: "blue",
              marginLeft: 10,
            }}
            onPress={this.Show}
          />
          <Text
            style={{
              flex: 2,
              height: 50,
              marginTop: 65,
              fontSize: 17,
              textAlign: "right",
            }}
            onPress={this.Show}
          >
            Don’t have a Account?
          </Text>
          <TouchableOpacity
            onPress={this.Show}
            style={{
              flex: 1,
              height: 50,
              marginTop: 65,
              fontWeight: "bold",
              fontSize: 17,
              marginLeft: 10,
              color: "#4ddbff",
            }}
          >
            <Text
              style={{
                flex: 1,
                height: 50,
                fontWeight: "bold",
                fontSize: 17,
                marginLeft: 10,
                color: "#4ddbff",
              }}
            >
              SIGNUP HERE
            </Text>
          </TouchableOpacity>
        </View>
        <Card
          style={{
            alignSelf: "center",
            marginTop: 200,
            radious: 10,
            cornerRadius: 40,
            padding: 50,
            borderRadius: 16,
          }}
          radious="40"
          onPress={this.Show}
        >
          <View>
            <Text
              style={{
                fontSize: 25,
                fontWeight: "bold",
                color: "darkblue",
              }}
            >
              Hi there,{"\n"}Welcome back!
            </Text>
            <Text
              style={{
                fontSize: 17,
                color: "grey",
                marginTop: 10,
              }}
            >
              Enter your Password to Login to Easiloan
            </Text>
            <TextInput
              style={{
                height: 50,
                margin: 12,
                borderWidth: 1,
                padding: 10,
                marginTop: 20,
                borderRadius: 8,
              }}
              placeholder="REGISTERED EMAIL ID"
              keyboardType="ascii-capable"
            />

            <TextInput
              style={{
                height: 50,
                margin: 12,
                borderWidth: 1,
                padding: 10,
                marginTop: 10,
                borderRadius: 8,
              }}
              placeholder="YOUR PASSWORD"
              keyboardType="visible-password"
            />

            <TouchableOpacity
              style={styles.SubmitButtonStyle}
              activeOpacity={0.5}
              onPress={this.Show}
            >
              <Text style={styles.TextStyle}> Login to Easiloan </Text>
            </TouchableOpacity>
            <View style={styles.forgetContainer}>
              <Text
                style={{
                  flex: 2,
                  height: 50,
                  fontSize: 17,
                  textAlign: "right",
                }}
              >
                Forgot Password?
              </Text>
              <Text
                style={{
                  flex: 2,
                  height: 50,
                  fontSize: 17,
                  textAlign: "left",
                  fontWeight: "bold",
                  color: "#4ddbff",
                  marginLeft: 10,
                }}
              >
                RESET HERE
              </Text>
            </View>
          </View>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    flexDirection: "row",
    height: 50,
  },
  forgetContainer: {
    flex: 1,
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 10,
  },
  SubmitButtonStyle: {
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: "#2929a3",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
    color: "#fff",
    alignItems: "center",
  },
  TextStyle: {
    color: "#fff",
    fontWeight: "bold",
  },
});
