import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";
import { Image } from "react-native";
import { Card } from "react-native-shadow-cards";

export default function App() {
  return (
    <View>
      <View style={styles.container}>
        {/* <Image
          source={require("./assets/Easy-Loan.jpg")}
          style={{
            flex: 1,
            height: 50,
            marginTop: 35,
            backgroundColor: "blue",
            marginLeft: 10,
          }}
        /> */}
        <Text
          style={{
            flex: 2,
            height: 50,
            marginTop: 65,
            fontSize: 17,
            textAlign: "right",
          }}
        >
          Already a User?
        </Text>
        <Text
          style={{
            flex: 1,
            height: 50,
            marginTop: 65,
            fontWeight: "bold",
            fontSize: 17,
            marginLeft: 10,
            color: "#4ddbff",
          }}
        >
          LOGIN HERE
        </Text>
      </View>
      <Card
        style={{
          alignSelf: "center",
          marginTop: 200,
          radious: 10,
          cornerRadius: 40,
          padding: 50,
          borderRadius: 16,
        }}
        radious="40"
      >
        <View>
          <Text
            style={{
              fontSize: 25,
              fontWeight: "bold",
              color: "darkblue",
            }}
          >
            Welcome to Easiloan’s Channel Partner Dashboard!
          </Text>
          <Text
            style={{
              fontSize: 17,
              color: "grey",
              marginTop: 10,
            }}
          >
            Enter your Organization Email ID to Create your Account
          </Text>
          <TextInput
            style={{
              height: 50,
              margin: 12,
              borderWidth: 1,
              padding: 10,
              marginTop: 20,
              borderRadius: 8,
            }}
            placeholder="YOUR NAME"
            keyboardType="ascii-capable"
          />

          <TextInput
            style={{
              height: 50,
              margin: 12,
              borderWidth: 1,
              padding: 10,
              marginTop: 10,
              borderRadius: 8,
            }}
            placeholder="REGISTERED EMAIL ID"
            keyboardType="email-address"
          />

          <TouchableOpacity
            style={styles.SubmitButtonStyle}
            activeOpacity={0.5}
          >
            <Text style={styles.TextStyle}> Login to Easiloan </Text>
          </TouchableOpacity>
        </View>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    flexDirection: "row",
    height: 50,
  },

  SubmitButtonStyle: {
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: "#2929a3",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
    color: "#fff",
    alignItems: "center",
  },
  TextStyle: {
    color: "#fff",
    fontWeight: "bold",
  },
});
